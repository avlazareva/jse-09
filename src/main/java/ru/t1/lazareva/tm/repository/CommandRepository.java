package ru.t1.lazareva.tm.repository;

import ru.t1.lazareva.tm.api.ICommandRepository;
import ru.t1.lazareva.tm.constant.ArgumentConst;
import ru.t1.lazareva.tm.constant.CommandConst;
import ru.t1.lazareva.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show command list.");

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version info.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show development info.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    private static Command[] COMMANDS = new Command[]{
            HELP, VERSION, INFO, ABOUT, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}